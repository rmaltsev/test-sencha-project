const express = require('express');
const app = express();
const morgan = require('morgan');
const settings = require('./settings.json');

app.use(morgan('combined'));
app.use(express.static('public'));

app.listen(settings.http.port, () => {
	console.log(`listening at port ${settings.http.port}`);
});
