FROM node:14.5.0-alpine
COPY ./ /app
WORKDIR /app
EXPOSE 8080
ENV NODE_ENV=stage
ENV NODE_OPTIONS=--enable-source-maps
ENTRYPOINT ["node", "./main.js"]
